<?php


namespace GuanChanghu\Utils\Arr;

use Illuminate\Support\Collection;

/**
 * Class Facade
 * @method static Collection generateTree(Collection $originalCollection, int $parentId = 0, string $primaryKey = 'id', string $parentKey = 'parentId')     生成tree
 * @method static void recursionArrayToCollection(array $array, Collection $collection)                                 递归 array to collection
 * @method static void recursionCollectionToArray(Collection $collection, array &$array)                                递归 collection to array
 * @method static void recursionKSort(array &$array)                                                                    递归 ksort 排序
 * @method static int|string|null|array|Collection getParameter(Collection $collection, array $param)                   按照循序返回指定的第一个参数
 * @method static Collection convertEmptyStringsToNull(Collection $collection, array $param)                            空字符串转化成null
 * @method static Collection merge(Collection $collect1, Collection $collect2, string $way = 'push')                    将$collect2融合到$collect1里
 * @method static array recursiveReplaceMerge(array $array1, array $array2)                                             递归替换融合
 * @package GuanChanghu\Utils\Arr
 */
class Facade extends Encrypt
{
}
