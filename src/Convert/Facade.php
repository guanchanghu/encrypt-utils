<?php


namespace GuanChanghu\Utils\Convert;


/**
 * Class Convert
 * @method static int chineseToNumberSimple(string $string)         翻译 一四五 => 145   , 自己写的简单翻译
 * @method static string numberToChineseSimple(int $number)         翻译 134 => 一三四   ，自己写的简单翻译
 * @method static string numberToChinese(int $number)               翻译 1.234 => 一点二三四 ， 使用chinese插件
 * @package GuanChanghu\Utils\Convert
 */
class Facade extends Encrypt
{

}
