<?php


namespace GuanChanghu\Utils\Math;


/**
 * Class Facade
 * @method static float|null bcCalculateByString(string $expression, int $digit = 2)            bc计算格式 1.3+0.6/2*(0.5+0.5+(1+1*2))
 * @method static float bcCalculateByArray(array $array, int $digit = 2)                        bc计算格式 array ( 0 => 1.3, 1 => '+', 2 => 0.6, 3 => '/', 4 => 2, 5 => '*', 6 => array ( 0 => 0.5, 1 => '+', 2 => 0.5, 3 => '+', 4 => array ( 0 => 1, 1 => '+', 2 => 1, 3 => '*', 4 => 2, ), ), )
 * @method static array stringToArray(string $expression)                                       格式转换 string => array
 * @method static string arrayToString(array $expression, int $level = 0)                       格式转换 array => string
 * @package GuanChanghu\Utils\Math
 */
class Facade extends Encrypt
{
}
