<?php

namespace GuanChanghu\Utils\Misc;

use Closure;

/**
 * @author 管昌虎
 * Class Facade
 * @method static string realIp()                               获得真实ip
 * @method static bool|string macAddress(string $interface = 'eth0')                               获得mac地址
 * @method static void dbListen(string $message = '', Closure|null $closure = null)               监听sql
 * @method static array implementingClasses(string $interfaceName)      获得实现接口的所有类，有缺陷 必须的加载的类才能检测出来
 * @package GuanChanghu\Utils\Misc
 * Created on 2023/4/15 11:42
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Facade extends Encrypt
{
}