<?php
/**
 * 随机工具门面类
 * Created on 2022/4/26 9:54
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 * @author 管昌虎
 */

namespace GuanChanghu\Utils\Random;

/**
 * @author 管昌虎
 * Class Facade
 * @method static string font()                             生成随机字体
 * @method static string number(int $length)                随机随机数字-第一个数字不是0
 * @method static string avatar()                           随机生成avatar
 * @method static int|string extract(array $proArr)         在数组当中抽取一项  抽奖方法
 * @package GuanChanghu\Utils\Random
 * Created on 2022/4/26 9:54
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class Facade extends Encrypt
{
}
