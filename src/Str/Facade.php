<?php


namespace GuanChanghu\Utils\Str;

use Illuminate\Support\Collection;

/**
 * Class Facade
 * @method static Collection rangeStrToCollection(string $string, int $limit = 100)         格式3-7,9-10,34-37 返回一个集合 如[3,4,5,6,7,9,10,34,35,36,37]
 * @method static string generateUuid(string $prefix = '')                                  根据前缀生成uuid
 * @method static string generateOrderId(string $prefix = '')                               根据前缀订单号
 * @method static string fileSuffix(string $file)                                           获得文件后缀
 * @package GuanChanghu\Utils\Str
 */
class Facade extends Encrypt
{
}
