<?php


namespace GuanChanghu\Utils\Time;

use Carbon\Carbon;
use Closure;
use DateTime;

/**
 * Class Facade
 * @method static float runningTime(Closure $closure)                               计算运行时间
 * @method static array runningTimeDetail(Closure $closure)                         计算运行时间-详情
 * @method static void sleep(string $date)                                          休眠到指定时间
 * @method static Carbon|false createByCarbon(string $format)                       创建指定格式的Carbon类 $format 2023-02-03 23:19:19
 * @method static Carbon|false getWeeHoursCarbon()                                  获得凌晨0点的Carbon类
 * @method static Carbon|false createByCarbonCompact(string $format)                创建指定格式的Carbon类 $format 20230203231919
 * @method static int getMillisecond(int $precision = 3)                            获得毫秒时间戳 返回实例 - 1596504903163
 * @method static float getMillisecondUnitSecond()                                  获得单位是秒的,毫秒级的时间戳 返回实例 - 1681050718.964
 * @method static string getFormatMillisecondTimestamp()                            获得格式化毫秒时间戳 返回实例 - 2020-08-03 16:48:19.811597
 * @method static string getSubMillisecondTimestamp(int $minute = 0)                获得sub格式化毫秒时间戳  如果传递10就是就是倒退10分钟  返回实例 - 2020-08-03 16:48:19.811597
 * @method static string getFormatTimestamp()                                       获得格式化时间戳 返回实例 - 2020-08-03 16:48:19
 * @method static string getFormatTimestampByDateTime(DateTime $dateTime)           获得格式化毫秒时间戳 返回实例 - 2020-08-03 16:48:19
 * @method static string getFormatDateYmd()                                         格式化时间年月日 返回实例 - 2020-08-03
 * @method static string getFormatDateYm()                                          格式化时间年月 返回实例 - 2020-08
 * @method static string getFormatDateY()                                           格式化时间年 返回实例 - 2020
 * @method static array getTime(...$params)                                         获得时间 传递 'Y','m' 返回 ['2023', '04']
 * @method static int getTimeYear()                                                 获得年
 * @method static string getTimeMonth()                                             获得月
 * @method static string getTimeDay()                                               获得日
 * @method static string getTimeHour()                                              获得时
 * @package GuanChanghu\Utils\Time
 */
class Facade extends Encrypt
{
}
